Flutter Grid UI
===============

## Flutter Grid UI implementation with [`dart_grid`](https://pub.dev/packages/dart_grid)

This package allows you to work with data tables like Excel.

## Getting Started

To use this package, add `flutter_grid_ui` as a [dependency in your pubspec.yaml file](https://flutter.io/platform-plugins/).

```yaml
dependencies:
  flutter_grid_ui: any
```

## Using

```dart
import 'package:flutter_grid_ui/flutter_grid_ui.dart';
```

## Example

![Example](https://gitlab.com/dipdev.studio/open-source/flutter-grid/-/raw/main/assets/example.gif)